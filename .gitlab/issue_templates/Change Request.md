<!--
Use this template to suggest small scale changes, like missing properties/functions or quality of life features.
If you want to suggest big features or major rewrite, please use the `Feature Request` template instead. 
-->

## Description
<!--
REQUIRED

Describe the changes you wish to propose, give a short motivation for why this should be added to Kord and
include links to the master branch classes that would be affected by this change.
-->

## Use-case
<!--
OPTIONAL

Provide a code snippet of how your suggestion would be used, if applicable.
-->

<!-- Quick actions -->
/label ~Change Request
