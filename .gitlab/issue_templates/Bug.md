<!-- 
Kord Bug Report template
You may delete all comment blocks before creating this issue or just keep them and write your content below them.
Sections marked as "OPTIONAL" can be ommited (don't forget to remove the header).
-->

## Summary

<!-- 
Provide a short summary of the issue 
-->

## Steps to reproduce 

<!-- 
Provide a minimal set of steps, code would be even better, to reproduce the bug. 
-->


## Expected Behavior 

<!-- 
What did you expect to happen. 
-->


## Actual Behavior 

<!-- 
What actually happened. Provide logs/stacktraces if applicable. 
-->


## Version

<!-- 
The exact version that contains this bug. 
-->

## Possible Fixes

<!-- 
OPTIONAL
 
If you can, link to the line of code that might be responsible for the problem. 
-->


<!-- Quick actions -->
/label ~Bug
