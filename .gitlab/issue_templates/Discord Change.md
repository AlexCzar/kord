<!-- 
Kord Discord change template
You may delete all comment blocks before creating this issue or just keep them and write your content below them.
All sections are required.
-->

## Description

<!-- 
An iteration of all fields added/removed/modified and their meanings 
-->

## Reference

<!--
Link to changes on GitHub 
-->


<!-- Quick actions -->
/label ~External Change
/label ~Help Wanted
