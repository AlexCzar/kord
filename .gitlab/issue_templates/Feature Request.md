<!--
Kord Feature Proposal template
You may delete all comment blocks before creating this issue or just keep them and write your content below them.

This template is meant to serve as a basic structure for new features that
intend to modify existing classes or introduce new functionality.
As such you may find yourself unable to fully fill out this form initially,
as you get feedback and a consensus is build you can revise it accordingly.

All sections are optional except those marked as "REQUIRED". Keep sections
in the order shown below. If an optional section is not needed then
simply omit it (don't forget to remove the header).
-->

## Summary

<!--
REQUIRED

Provide a one or two sentence description of the feature proposal.
-->


## Goals

<!--
What are the goals of this proposal?  Omit this section if you have
nothing to say beyond what's already in the summary.
-->

## Non-Goals

<!--
Describe any goals you wish to identify specifically as being out of
scope for this proposal.
-->

## Motivation

<!--
Why should this work be done?  What are its benefits? Who's asking for it? 
-->

## Description

<!--
REQUIRED

Provide a detailed description of the feature: What it is, and how you intend to implement it.

Summarize, at a high level, the part of the API you expect to modify or extend,
including internal APIs. Describe any issues you can think of with this implementation.

Include hyperlinks to additional documents as required.
-->

## Alternatives

<!--
Did you consider any alternative approaches?  
If so please describe them here and explain why they were not chosen.
-->

## Dependencies

<!--
If any, describe the dependencies the feature proposal has on other open issues.
-->


<!-- Quick actions -->
/label ~Feature
