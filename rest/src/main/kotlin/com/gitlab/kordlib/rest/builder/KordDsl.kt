package com.gitlab.kordlib.rest.builder

@DslMarker
@Target(AnnotationTarget.CLASS)
annotation class KordDsl